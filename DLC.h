/*
 * DLC.h - HDLC like serial protocol
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DLC_H_
#define DLC_H_

#include <stddef.h>
#include <stdint.h>

#include "Arduino.h"

/**
 * DLC frame parser. This parser is based on the PPP over HDLC frames but
 * removes address and command in favor of a length field. Empty and invalid
 * frames are discarded.
 */
class DLC {
private:
	uint8_t* buffer_;
	size_t max_len_;

	typedef enum {
		DLC_STATE_IDLE,
		DLC_STATE_LENGTH,
		DLC_STATE_DATA,
		DLC_STATE_FCS,
	} state_t;

	state_t state_;
	unsigned int offset_;
	bool escaped_;

	uint32_t crc_;
	
	unsigned int len_;
	uint32_t fcs_;

	unsigned int fcs_error_count_;

	void set_state(state_t next);

	void update_crc(uint8_t value);
	static uint32_t calc_crc(uint32_t crc, const uint8_t* buffer, size_t len);

	static void write_byte(Serial_& serial, uint8_t value);
	static uint32_t write_block(Serial_& serial, uint32_t crc, const uint8_t* buffer, size_t len);

public:
	/**
	 * Writes buffers into a DLC frame. A @c NULL buffer ends the buffer
	 * chain.
	 */
	static void write(Serial_& serial, ... /* void* buffer, size_t len */);

	DLC(void* buffer, size_t max_len);

	/**
	 * Reads as much data as available, but nor more than necessary to
	 * complete this frame from the given stream.
	 *
	 * Invalid frames are discarded and won't return @c true.
	 *
	 * @return whether the frame is complete and valid
	 */
	bool read(Stream& stream);

	void* get_data(void) const { return buffer_; }
	unsigned int get_length(void) const { return len_; }
	unsigned int get_fcs_error_count(void) const { return fcs_error_count_; }
};


#endif /* DLC_H_ */

