/*
 * dig_input.cpp - Digital input
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "dig_input.h"

DigInput::DigInput(void) :
	Input(),
	high_(),
	period_()
{
}

DigInput::~DigInput(void)
{
}

void DigInput::add_edges(uint32_t tsA, uint32_t tsB)
{
	if (is_failed() || is_reset()) {
		/* Restart capture. We synchronize on the falling edge */
		clear_failed();
		clear_reset();
	} else {
		uint32_t deltaA, deltaB;

		deltaA = tsA - last_edge_;
		deltaB = tsB - tsA;

		period_.add(deltaA + deltaB, last_edge_);
		high_.add(deltaB, tsA);
	}

	last_edge_ = tsB;
}

