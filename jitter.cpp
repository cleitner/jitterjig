/*
 * jitter.cpp - JitterJig library
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "jitter.h"

#include "Arduino.h"

Jitter_ Jitter;

const uint32_t JITTER_NS_PER_TIME_UNIT = 125;


static const uint32_t ACK = 0x06;
static const uint32_t NAK = 0x15;

struct IRQLock {
	IRQLock(void) { __disable_irq(); }
	~IRQLock(void) { __enable_irq(); }
};

extern void setupSystemClock(void)
{
	// We reuse PLLA and tune it to 16 * 12. The main clock is then divided
	// by 3 which gives us 64 MHz.

	// Switch to oscillator
 	PMC->PMC_MCKR = (PMC->PMC_MCKR & ~PMC_MCKR_CSS_Msk) | PMC_MCKR_CSS_MAIN_CLK;
	while (!(PMC->PMC_SR & PMC_SR_MCKRDY)) ;

	// Change PLLA
	PMC->CKGR_PLLAR = CKGR_PLLAR_ONE;
	PMC->CKGR_PLLAR =
		CKGR_PLLAR_ONE |
		CKGR_PLLAR_MULA(0xfUL) |
		CKGR_PLLAR_PLLACOUNT(0x3fUL) |
		CKGR_PLLAR_DIVA(0x1UL);
	while (!(PMC->PMC_SR & PMC_SR_LOCKA)) ;

	// Change prescaler
	PMC->PMC_MCKR = (PMC->PMC_MCKR & ~PMC_MCKR_PRES_Msk) | PMC_MCKR_PRES_CLK_3;
	while (!(PMC->PMC_SR & PMC_SR_MCKRDY)) ;

	// Switch to PLLA
	PMC->PMC_MCKR = (PMC->PMC_MCKR & ~PMC_MCKR_CSS_Msk) | PMC_MCKR_CSS_PLLA_CLK;
	while (!(PMC->PMC_SR & PMC_SR_MCKRDY)) ;

	SystemCoreClockUpdate();

	SysTick_Config(SystemCoreClock / 1000);
}

extern void measurementChanged(Measurement* measurement)
{
	Jitter.on_measurement_changed(measurement);
}


extern "C" void TC3_Handler(void)
{
	TC_GetStatus(TC1, 0);
	Jitter.can_output_.send();
}

extern "C" void TC6_Handler(void)
{
	Jitter.dig_input2_.update();
}

extern "C" void TC7_Handler(void)
{
	Jitter.dig_input1_.update();
}

extern "C" void CAN0_Handler(void)
{
	Jitter.can_output_.on_irq();
}

extern "C" void CAN1_Handler(void)
{
	// Take the timer based timestamp ASAP
	uint32_t timestamp = TC2->TC_CHANNEL[0].TC_CV;

	Jitter.can_input_.on_irq(timestamp);
}

Jitter_::Delay::Delay(void) :
	from_index(JITTER_MEASUREMENT_DELAY1),
	to_index(JITTER_MEASUREMENT_DELAY1),
	from(NULL),
	to(NULL),
	delay(),
	last_edge(0)
{
}

void Jitter_::send_ack(Serial_& s)
{
	DLC::write(s, &dlc_msg_[0], 4, &ACK, 4, NULL);
}

void Jitter_::send_nak(Serial_& s)
{
	DLC::write(s, &dlc_msg_[0], 4, &NAK, 4, NULL);
}

void Jitter_::on_measurement_changed(Measurement* measurement)
{
	uint32_t ts = measurement->get_last_change();

	for (int n = 0; n < JITTER_DELAY_COUNT_; n++) {
		Delay& d = delays_[n];

		if (measurement == d.from) {
			d.last_edge = ts;
		}

		if (measurement == d.to) {
			// Skip the delay update if last_edge is 0 (reset state)
			if (d.last_edge > 0) {
				d.delay.add(ts - d.last_edge, ts);
				d.last_edge = 0;
			}
		}
	}
}

Jitter_::Jitter_(void) :
	delays_(),
	can_output_(CAN0),
	can_input_(CAN1),
	dig_output_(),
	dig_input1_(),
	dig_input2_(),
	dlc_(dlc_msg_, sizeof(dlc_msg_))
{
	for (int d = 0; d < JITTER_DELAY_COUNT_; d++) {
		measurements_[JITTER_MEASUREMENT_DELAY1 + d] = &delays_[d].delay;
	}
	measurements_[JITTER_MEASUREMENT_DIG1_HIGH] = &dig_input1_.get_high();
	measurements_[JITTER_MEASUREMENT_DIG1_PERIOD] = &dig_input1_.get_period();
	measurements_[JITTER_MEASUREMENT_DIG2_HIGH] = &dig_input2_.get_high();
	measurements_[JITTER_MEASUREMENT_DIG2_PERIOD] = &dig_input2_.get_period();
	measurements_[JITTER_MEASUREMENT_CAN_INTERVAL] = &can_input_.get_interval();

	// Set all histograms to 10ms with a bin width of 5us
	for (int n = 0; n < JITTER_MEASUREMENT_COUNT_; n++) {
		measurements_[n]->get_histogram().setup(
			10000000 / JITTER_NS_PER_TIME_UNIT,
			5000 / JITTER_NS_PER_TIME_UNIT);
	}

	for (int d = 0; d < JITTER_DELAY_COUNT_; d++) {
		delays_[d].from_index = JITTER_MEASUREMENT_DIG1_HIGH;
		delays_[d].to_index = JITTER_MEASUREMENT_DIG2_HIGH;

		delays_[d].from = measurements_[JITTER_MEASUREMENT_DIG1_HIGH];
		delays_[d].to = measurements_[JITTER_MEASUREMENT_DIG2_HIGH];
	}
}

void Jitter_::begin(void)
{
	setupSystemClock();

	// -- Interrupt Priorities --
	// Change all priorites to the maximum value (lowest priority)
	NVIC_SetPriority(SysTick_IRQn, 15);
	for (int n = 0; n < PERIPH_COUNT_IRQn; n++) {
		NVIC_SetPriority((IRQn_Type) n, 15);
	}
	// And boost the CAN and TC3 interrupts
	NVIC_SetPriority(TC3_IRQn, 0);
	NVIC_SetPriority(CAN0_IRQn, 0);
	NVIC_SetPriority(CAN1_IRQn, 0);

	// -- Digital I/O --

	// TODO: move the Due specific I/O routing here (dig inputs/output)
	dig_output_.begin();

	dig_input1_.begin();
	dig_input2_.begin();

	// -- CAN --

	// CANTX0: PA0/A
	// CANRX0: PA1/A
	pmc_enable_periph_clk(ID_PIOA);
	PIO_SetOutput(PIOA, PIO_PA0, 1, 1, 1);
	PIO_SetInput(PIOA, PIO_PA1, PIO_PULLUP);
	PIO_SetPeripheral(PIOA, PIO_PERIPH_A, PIO_PA0 | PIO_PA1);

	pmc_enable_periph_clk(ID_CAN0);

	can_output_.begin(CAN_BAUDRATE_1MBPS);

	pmc_enable_periph_clk(ID_TC3);
	TC_Configure(TC1, 0, TC_CMR_TCCLKS_TIMER_CLOCK2 |
		TC_CMR_CPCTRG |
		TC_CMR_LDRA_NONE |
		TC_CMR_LDRB_NONE);

	// Trigger the CAN output every 1ms
	(TC1->TC_CHANNEL + 0)->TC_RC = 1000000 / JITTER_NS_PER_TIME_UNIT;

	(TC1->TC_CHANNEL + 0)->TC_IER = TC_IER_CPCS;
	NVIC_EnableIRQ(TC3_IRQn);

	TC_Start(TC1, 0);

	// CANTX1: PB14/A
	// CANRX1: PB15/A
	pmc_enable_periph_clk(ID_PIOB);
	PIO_SetOutput(PIOB, PIO_PB14, 1, 0, 1);
	PIO_SetInput(PIOB, PIO_PB15, PIO_PULLUP);
	PIO_SetPeripheral(PIOB, PIO_PERIPH_A, PIO_PB14 | PIO_PB15);

	pmc_enable_periph_clk(ID_CAN1);

	can_input_.begin(CAN_BAUDRATE_1MBPS);

	NVIC_EnableIRQ(CAN1_IRQn);

	// -- Sync --

	// Sync all timers under control
	{
		IRQLock irq_lock;

		TC1->TC_BCR = TC_BCR_SYNC;
		TC2->TC_BCR = TC_BCR_SYNC;
	}
}


void Jitter_::reset_all(void) {
	for (int n = 0; n < JITTER_MEASUREMENT_COUNT_; n++) {
		reset((jitter_measurement_t) n);
	}
}

void Jitter_::reset(jitter_measurement_t m) {
	if (m >= JITTER_MEASUREMENT_COUNT_) {
		return;
	}

	IRQLock irq_lock;

	measurements_[m]->clear();
}

Measurement Jitter_::get_measurement(jitter_measurement_t m) {
	if (m >= JITTER_MEASUREMENT_COUNT_) {
		return Measurement();
	}

	IRQLock irq_lock;

	return *measurements_[m];
}

void Jitter_::setup_histogram(jitter_measurement_t m, uint32_t center, uint32_t bin_width)
{
	if (m >= JITTER_MEASUREMENT_COUNT_ || bin_width == 0) {
		return;
	}

	IRQLock irq_lock;

	measurements_[m]->get_histogram().setup(center, bin_width);
}

bool Jitter_::setup_delay(jitter_delay_t delay, jitter_measurement_t from, jitter_measurement_t to) {
	if (from >= JITTER_MEASUREMENT_COUNT_ ||
		to >= JITTER_MEASUREMENT_COUNT_ ||
		from < JITTER_DELAY_COUNT_ ||
		to < JITTER_DELAY_COUNT_)
	{
		return false;
	}

	Delay& d = delays_[delay];

	d.from_index = from;
	d.to_index = to;

	IRQLock irq_lock;

	d.from = measurements_[from];
	d.to = measurements_[to];

	d.delay.clear();

	return true;
}

void Jitter_::get_delay_settings(jitter_delay_t delay, jitter_measurement_t* from, jitter_measurement_t* to)
{
	if (delay >= JITTER_DELAY_COUNT_) {
		return;
	}

	*from = delays_[delay].from_index;
	*to = delays_[delay].to_index;
}

void Jitter_::loop(Serial_& s)
{
	if (!dlc_.read(s)) {
		return;
	}

	unsigned int len = dlc_.get_length();
	if (len < 4) {
		return;
	}

	// We received a message. Lets see how we can handle that. Our protocol
	// works entirely with 32 bit values (at least on the incoming side), so
	// no need to pack structures etc.
	switch (dlc_msg_[0]) {
	// Version of protocol/application and timebase
	case 'V': {
		uint32_t version = 0x00020000;
		uint32_t delays = JITTER_DELAY_COUNT_;
		uint32_t measurements = JITTER_MEASUREMENT_COUNT_;
		DLC::write(s,
			&dlc_msg_[0], 4, &ACK, 4,

			&version, 4,
			&JITTER_NS_PER_TIME_UNIT, 4,
			&delays, 4,
			&measurements, 4,
			NULL);
		break;
	}

	// Reset all measurements
	case 'R':
		if (len >= 8) {
			if (dlc_msg_[1] >= JITTER_MEASUREMENT_COUNT_) {
				send_nak(s);
			} else {
				reset((jitter_measurement_t) dlc_msg_[1]);
				send_ack(s);
			}
		} else {
			reset_all();
			send_ack(s);
		}
		break;

	// Returns the measurements meta info
	case 'M': {
		if (len < 8 || dlc_msg_[1] >= JITTER_MEASUREMENT_COUNT_) {
			send_nak(s);
			break;
		}

		MeasurementInfo info = get_measurement_info((jitter_measurement_t) dlc_msg_[1]);

		DLC::write(s,
			&dlc_msg_[0], 4, &ACK, 4,

			&info.bin_count, 4,
			info.name, strlen(info.name),

			NULL);

		break;
	}


	// Returns a snapshot of the given measurement
	case 'S': {
		if (len < 8 || dlc_msg_[1] >= JITTER_MEASUREMENT_COUNT_) {
			send_nak(s);
			break;
		}
		Measurement m = get_measurement((jitter_measurement_t) dlc_msg_[1]);

		uint32_t count = m.get_count();
		uint32_t mean = m.get_mean();
		uint32_t min = m.get_min();
		uint32_t max = m.get_max();
		uint32_t last = m.get_last_value();
		uint32_t failures = m.get_fail_count();

		Histogram<>& h = m.get_histogram();

		uint32_t center = h.get_center();
		uint32_t bin_width = h.get_bin_width();
		uint32_t lower = h.get_lower();
		uint32_t higher = h.get_higher();

		DLC::write(s,
			&dlc_msg_[0], 4, &ACK, 4,

			&count, 4,
			&mean, 4,
			&min, 4,
			&max, 4,
			&last, 4,
			&failures, 4,

			&center, 4,
			&bin_width, 4,
			&lower, 4,
			&higher, 4,
			h.begin(), (h.end() - h.begin()) * 4,

			NULL);
		break;
	}

	// Configures a histogram
	case 'H': {
		if (len < 16 ||
			dlc_msg_[1] >= JITTER_MEASUREMENT_COUNT_  ||
			dlc_msg_[3] == 0) {
			send_nak(s);
			break;
		}

		setup_histogram((jitter_measurement_t) dlc_msg_[1], dlc_msg_[2], dlc_msg_[3]);
		send_ack(s);
		break;
	}

	// Configures (and resets) the delay measurement
	case 'D': {
		if (len < 16) {
			send_nak(s);
			break;
		}

		if (setup_delay(
			(jitter_delay_t) dlc_msg_[1],
			(jitter_measurement_t) dlc_msg_[2],
			(jitter_measurement_t) dlc_msg_[3])) {
			send_ack(s);
		} else {
			send_nak(s);
		}
		break;
	}

	// Returns the delay settings
	case 'G': {
		if (len < 8 || dlc_msg_[1] >= JITTER_DELAY_COUNT_) {
			send_nak(s);
			break;
		}

		jitter_measurement_t mfrom = JITTER_MEASUREMENT_DELAY1, mto = JITTER_MEASUREMENT_DELAY1;
		get_delay_settings((jitter_delay_t) dlc_msg_[1], &mfrom, &mto);

		uint32_t from = mfrom;
		uint32_t to = mto;

		DLC::write(s,
			&dlc_msg_[0], 4, &ACK, 4,
			&from, 4,
			&to, 4,
			NULL);
		break;
	}

	default:
		send_nak(s);
		break;
	}
}

MeasurementInfo Jitter_::get_measurement_info(jitter_measurement_t m) const
{
	switch (m) {
	case JITTER_MEASUREMENT_DELAY1: return MeasurementInfo("Delay 1", *measurements_[m]);
	case JITTER_MEASUREMENT_DELAY2: return MeasurementInfo("Delay 2", *measurements_[m]);
	case JITTER_MEASUREMENT_DELAY3: return MeasurementInfo("Delay 3", *measurements_[m]);
	case JITTER_MEASUREMENT_DELAY4: return MeasurementInfo("Delay 4", *measurements_[m]);
	case JITTER_MEASUREMENT_DIG1_HIGH: return MeasurementInfo("Digital Input 1 High", *measurements_[m]);
	case JITTER_MEASUREMENT_DIG1_PERIOD: return MeasurementInfo("Digital Input 1 Period", *measurements_[m]);
	case JITTER_MEASUREMENT_DIG2_HIGH: return MeasurementInfo("Digital Input 2 High", *measurements_[m]);
	case JITTER_MEASUREMENT_DIG2_PERIOD: return MeasurementInfo("Digital Input 2 Period", *measurements_[m]);
	case JITTER_MEASUREMENT_CAN_INTERVAL: return MeasurementInfo("CAN Interval", *measurements_[m]);
	default: return MeasurementInfo("", 1);
	}
}


