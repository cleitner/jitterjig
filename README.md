JitterJig
=========

The JitterJig is a tool to measure jitter in CAN bus and digital applications.
It comes with a handy GUI to observe these measurements in real-time.

It's intended to be used as a regular measurement tool and for automated
regression testing.


Prerequisites
-------------

To run the PC tools you'll need a Python3 interpreter with pySerial installed.

As hardware platform the JitterJig uses the Arduino Due. Plug the Dues native
USB port into your PC and start `gui/jitterjig.py`.

Copy all source files to `$HOME/Arduino/libraries/jitterjig`. The source files
in `usb/` have to be copied into your Arduino base library
`hardware/arduino/sam/cores/arduino/USB` to override the buggy originals.


Inputs
------

Digital inputs are available on pins PWM3 (Input 1) and PWM5 (Input 2). Two
test signals are available on pins PWM11 and PWM12.

To use the CAN input, you'll need a CAN transceiver for the CAN1 peripheral on
pins DAC0 and 53. A CAN output is available through CAN0 on pins CANRX and
CANTX.

CAN timings are based on the CANopen SYNC frame (ID 0x80).

![JitterJig on Arduino Due](docs/jitterjig_due.png)

Timing
------

Digital timings are realized by the timer peripheral of the microcontroller and
have no superimposed jitter from the software. Timings are based on the 125 ns
granularity of the timer.

CAN timings are currently based on the CAN clock, assisted by the digital timer
for delay measurements. The CAN clocks resolution is the inverse of the baud
rate, e.g. 1 µs for 1000 kBit/s. As the Arduino has no way of sampling the
digital timer based on the reception of a CAN frame, those measurements are
subject to small jitter based on the IRQ timing.

To support correct CAN timings, the system clock frequency on the Due had to be
slowed down to 64 MHz. As a direct consequence, the JitterJig library has to be
initialized first to ensure that other peripherals can base their timings on
the new system clock.


USB related fixes
-----------------

Currently (1.5.6-r2) the Arduino USB CDC interface has severe reliability
problems and a group of users created a patch to fix them. As an addition I
added an implementation of `LockEP`, which avoids disabling all interrupts
where disabling the USB IRQ would suffice.


License
-------

The main code is licensed under the GPL-2.0+. All sources below `usb/` use the
regular Arduino license, as they're derivates of the official source bundle.

