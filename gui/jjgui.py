#!/usr/bin/env python3

# jjgui.py - GUI for the JitterJig
#
# Copyright (c) 2014 Colin Leitner
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import jitterjig
import random
import serial
import math

import tkinter as tk
import tkinter.ttk as ttk
import effbot

class Style(dict):
	__getattr__= dict.__getitem__

STYLE = Style()
STYLE.background = "#FFFFFF"
STYLE.foreground = "#000000"
STYLE.neutral = "#EEEEEE"
STYLE.neutral = "#FFFFFF"
STYLE.highlight = "#008EFF"
STYLE.special1 = "#008EFF"
STYLE.special2 = "#CCCCCC"
STYLE.error = "#DD0000"

root = tk.Tk()

for i in [ "bins_wider", "bins_narrower", "center_left", "center_right", "center_mean", "reset", "open", "close", "save" ]:
	STYLE[i + "_active"] = tk.PhotoImage(file="imgs/" + i + "_active.gif")
	STYLE[i + "_inactive"] = tk.PhotoImage(file="imgs/" + i + "_inactive.gif")

root.configure(bg=STYLE.background)
root.title("JitterJig")

class OpenDeviceDialog(effbot.Dialog):
	def __init__(self, parent, ports):
		self._ports = ports

		effbot.Dialog.__init__(self, parent, title="Select Device COM port")

	def body(self, master):
		ttk.Label(master, text="COM Port:").pack()
		self._selection = ttk.Combobox(master, values=self._ports)
		self._selection.pack(padx=5)
		if len(self._ports) > 0:
			self._selection.current(0)

	def validate(self):
		return len(self._selection.get().strip()) > 0

	def apply(self):
		self.result = self._selection.get().strip()

ports = []
try:
	import serial.tools.list_ports
	ports = []
	for port, _, _ in serial.tools.list_ports.comports():
		ports.append(port)
except:
	pass

if len(ports) == 0:
	import glob
	# Poor mans serial port detection
	ports = sorted(glob.glob("/dev/ttyACM*") + glob.glob("/dev/ttyUSB*") + glob.glob("/dev/ttyS*") + glob.glob("/dev/cu.*"))

d = OpenDeviceDialog(root, ports)
if d.result is None:
	import sys
	sys.exit(1)

port = serial.Serial(d.result, 115200, timeout=1)
j = jitterjig.Jitter(port)

jversion = j.getVersion()
jmeasurement = j.getMeasurement(0)
jbin_count = len(jmeasurement.histogram.bins)

def format_time(units):
	if units == 0xFFFFFFFF:
		return "-"

	t = units * jversion.ns_per_unit
	at = abs(t)

	unit = ""
	if at < 1000:
		unit = " ns"
	elif at < 1e6:
		t = t / 1e3
		unit = " us"
	elif at < 1e9:
		t = t / 1e6
		unit = " ms"
	else:
		t = t / 1e9
		unit = " s"

	return "{:.3f}{}".format(t, unit)

toolbar = tk.Canvas(root, height=27, bg=STYLE.background, highlightthickness=0, bd=0)
toolbar.pack(anchor=tk.NW, fill=tk.Y)

body = tk.Frame(root, bg=STYLE.background)
body.pack(expand=True, fill=tk.BOTH)


scroll = tk.Scrollbar(body)
scroll.pack(side=tk.RIGHT, fill=tk.Y)
flow = tk.Text(body, relief=tk.FLAT, spacing1=8, spacing2=8, cursor="left_ptr")
flow.pack(expand=True, fill=tk.BOTH)

flow.configure(yscrollcommand=scroll.set)
scroll.configure(command=flow.yview)


def bind_click(widget, tag, handler):
	"""
	Adds a "proper" click handler to a canvas item by reacting only to
	button 1 release events if the mouse is still inside. This allows the
	user to abort the click
	"""
	state = {}
	state["inside"] = False

	def on_click(evt):
		if state["inside"]:
			handler(evt)

	def on_enter(evt):
		state["inside"] = True

	def on_leave(evt):
		state["inside"] = False

	widget.tag_bind(tag, '<ButtonRelease-1>', on_click)
	widget.tag_bind(tag, '<Enter>', on_enter)
	widget.tag_bind(tag, '<Leave>', on_leave)

def add_button(widget, x, y, image, handler):
	btn = widget.create_image(
		x, y,
		anchor=tk.NW,
		activeimage=STYLE[image + "_active"],
		image=STYLE[image + "_inactive"])
	bind_click(widget, btn, handler)
	return btn

class HistogramPanel(tk.Canvas):
	def __init__(self, root, index, bin_count):
		self._index = index
		self._measurement = None
		self._bin_count = bin_count
		self._width = 3 * (bin_count + 2) + 1 * ((bin_count + 2) - 1)
		self._height = int(round(self._width / 1.618)) + 48

		tk.Canvas.__init__(self, root,
			width=self._width,
			height=self._height,
			highlightthickness=0,
			bd=0,
			bg=STYLE.neutral)

		self.create_rectangle(
			int((bin_count + 2) / 2) * 4, 0,
			int((bin_count + 2) / 2) * 4 + 3, self._height - 48,
			outline="",
			fill=STYLE.special2)

		self._bins = []
		for n in range(bin_count + 2):
			f = None
			if n == 0 or n == bin_count + 1:
				f = STYLE.special1
			else:
				f = STYLE.foreground

			h = 1

			bar = self.create_rectangle(
				n * 4, self._height - 48 - h,
				n * 4 + 3, self._height - 48,
				outline="",
				fill=f)

			if n == 0:
				self._lower = bar
			elif n == bin_count + 1:
				self._higher = bar
			else:
				self._bins.insert(n - 1, bar)

		self._txt_lower = self.create_text(
			0, self._height - 44,
			font=("sans-serif", 8),
			anchor=tk.NW,
			text="")

		self._txt_higher = self.create_text(
			self._width, self._height - 44,
			font=("sans-serif", 8),
			anchor=tk.NE,
			text="")

		self._txt_center = self.create_text(
			self._width / 2, self._height - 44,
			font=("sans-serif", 8),
			anchor=tk.N,
			text="")

		add_button(self, 0, self._height - 27, "bins_wider", self._on_bins_wider)
		add_button(self, 27, self._height - 27, "bins_narrower", self._on_bins_narrower)

		add_button(self, self._width / 2 - 23 / 2 - 27, self._height - 27, "center_left", self._on_center_left)
		add_button(self, self._width / 2 - 23 / 2, self._height - 27, "center_mean", self._on_center_mean)
		add_button(self, self._width / 2 - 23 / 2 + 27, self._height - 27, "center_right", self._on_center_right)

	def _on_bins_wider(self, evt):
		if self._measurement is None:
			return

		width = self._measurement.histogram.bin_width
		if width < 5:		# ~1 us
			width = 5
		elif width < 50:	# ~10 us
			width = 50
		elif width < 500:	# ~100 us
			width = 500
		elif width < 5000:	# ~1 ms
			width = 5000
		elif width < 50000:	# ~10 ms
			width = 50000
		else:			# ~100 ms
			width = 500000

		j.setHistogram(self._index, self._measurement.histogram.center, width)
		update_panel(self._index)

	def _on_bins_narrower(self, evt):
		if self._measurement is None:
			return

		width = self._measurement.histogram.bin_width
		if width > 500000:
			width = 500000
		elif width > 50000:
			width = 50000
		elif width > 5000:
			width = 5000
		elif width > 500:
			width = 500
		elif width > 50:
			width = 50
		else:
			width = 5

		j.setHistogram(self._index, self._measurement.histogram.center, width)
		update_panel(self._index)

	def _on_center_left(self, evt):
		if self._measurement is None:
			return
		j.setHistogram(self._index, max(0, self._measurement.histogram.center - self._measurement.histogram.bin_width * 10), self._measurement.histogram.bin_width)
		update_panel(self._index)

	def _on_center_right(self, evt):
		if self._measurement is None:
			return
		j.setHistogram(self._index, min(0xFFFFFFFF, self._measurement.histogram.center + self._measurement.histogram.bin_width * 10), self._measurement.histogram.bin_width)
		update_panel(self._index)

	def _on_center_mean(self, evt):
		if self._measurement is None:
			return
		j.setHistogram(self._index, self._measurement.mean, self._measurement.histogram.bin_width)
		update_panel(self._index)

	def _bar_height(self, v, max_count):
		height = self._height - 48

		if v == 0:
			return 1

		# Linear scale is kind of worthless with lots of samples
		digits = max(1, math.ceil(math.log10(max_count)))

		return (math.log10(v) + 1) * height / (digits + 1)

	def update(self, measurement):
		self._measurement = measurement
		histogram = measurement.histogram

		assert len(histogram.bins) == len(self._bins)

		self.itemconfigure(self._txt_lower, text=format_time(-histogram.bin_width * len(histogram.bins) / 2))
		self.itemconfigure(self._txt_higher, text=format_time(histogram.bin_width * len(histogram.bins) / 2))
		self.itemconfigure(self._txt_center, text=format_time(histogram.center))

		max_count = max(histogram.bins)
		max_count = max(max_count, 1)
		for n in range(len(self._bins)):
			h = self._bar_height(histogram.bins[n], max_count)
			self.coords(self._bins[n],
				(n + 1) * 4, self._height - 48 - h,
				(n + 1) * 4 + 3, self._height - 48)

		h = self._bar_height(histogram.lower, max_count)
		#h = max(1, (self._height - 48) * min(histogram.lower / max_count, 1))
		self.coords(self._lower,
			0 * 4, self._height - 48 - h,
			0 * 4 + 3, self._height - 48)
		
		h = self._bar_height(histogram.higher, max_count)
		#h = max(1, (self._height - 48) * min(histogram.higher / max_count, 1))
		self.coords(self._higher,
			(len(self._bins) + 1) * 4, self._height - 48 - h,
			(len(self._bins) + 1) * 4 + 3, self._height - 48)






class MeasurementPanel(tk.Frame):
	def __init__(self, root, index, minfos):
		tk.Frame.__init__(self, root, bg=STYLE.background, padx=8, pady=8)
		self.bind("<Enter>", lambda evt: self.configure(bg=STYLE.special2))
		self.bind("<Leave>", lambda evt: self.configure(bg=STYLE.background))

		self.index = index
		self.open = True
		self.measurement = None

		title = tk.Frame(self, bg=STYLE.background, pady=4)
		title.grid()
		tk.Label(title, text=minfos[index].name, width=32, anchor=tk.W, bg=STYLE.background, font=("sans-serif", 16)).pack(side=tk.LEFT, expand=True, fill=tk.BOTH)

		self._buttons = tk.Canvas(title, width=50, height=23, bg=STYLE.background, highlightthickness=0)
		self._buttons.pack(side=tk.RIGHT)

		add_button(self._buttons, 0, 0, "reset", self._on_reset)
		self._btn_open_close = add_button(self._buttons, 27, 0, "close", self._on_open_close)

		hr = tk.Frame(self, bg=STYLE.highlight, height=1)
		hr.grid(sticky=tk.EW)

		body = tk.Frame(self, bg=STYLE.background, pady=4)
		body.grid()

		self._body = body

		self._form = tk.Frame(body, bg=STYLE.background, padx=4)
		self._form.grid(column=0, row=0, sticky=tk.NSEW)

		bg = STYLE.background

		tk.Label(self._form, text="Samples:", bg=bg).grid(sticky=tk.NE)
		tk.Label(self._form, text="Mean:", bg=bg).grid(sticky=tk.NE)
		tk.Label(self._form, text="Min:", bg=bg).grid(sticky=tk.NE)
		tk.Label(self._form, text="Max:", bg=bg).grid(sticky=tk.NE)
		tk.Label(self._form, text="Failures:", bg=bg).grid(sticky=tk.NE)

		self._lbl_samples  = tk.Label(self._form, text="0", width=14, anchor=tk.E, bg=bg)
		self._lbl_samples  .grid(column=1, row=0, sticky=tk.NE)
		self._lbl_mean     = tk.Label(self._form, text="0", width=14, anchor=tk.E, bg=bg)
		self._lbl_mean     .grid(column=1, row=1, sticky=tk.NE)
		self._lbl_min      = tk.Label(self._form, text="0", width=14, anchor=tk.E, bg=bg)
		self._lbl_min      .grid(column=1, row=2, sticky=tk.NE)
		self._lbl_max      = tk.Label(self._form, text="0", width=14, anchor=tk.E, bg=bg)
		self._lbl_max      .grid(column=1, row=3, sticky=tk.NE)
		self._lbl_failures = tk.Label(self._form, text="0", width=14, anchor=tk.E, bg=bg)
		self._lbl_failures .grid(column=1, row=4, sticky=tk.NE)

		border = tk.Frame(body, padx=4, bg=STYLE.neutral)
		border.grid(column=1, row=0)
		self._histogram = HistogramPanel(border, index, minfos[index].bin_count)
		self._histogram.pack()

	def _on_open_close(self, evt):
		self.open = not self.open
		if not self.open:
			self._buttons.itemconfigure(self._btn_open_close,
				activeimage=STYLE.open_active,
				image=STYLE.open_inactive)
			self._body.grid_remove()
		else:
			self._buttons.itemconfigure(self._btn_open_close,
				activeimage=STYLE.close_active,
				image=STYLE.close_inactive)
			self._body.grid()
			update_panel(self.index)

	def _on_reset(self, evt):
		j.reset(self.index)
		update_panel(self.index)

	def update(self, measurement):
		self.measurement = measurement

		self._histogram.update(measurement)

		self._lbl_samples.configure(text=measurement.count)
		self._lbl_mean.configure(text=format_time(measurement.mean))
		self._lbl_min.configure(text=format_time(measurement.min))
		self._lbl_max.configure(text=format_time(measurement.max))
		self._lbl_failures.configure(text=measurement.failures)



class DelayPanel(MeasurementPanel):
	def __init__(self, root, index, minfos, delays):
		MeasurementPanel.__init__(self, root, index, minfos)

		self._delay_count = delays

		delay_measurements = [ minfos[m].name for m in range(len(minfos)) if m >= delays ]

		bg = STYLE.background

		tk.Label(self._form, text="From:", bg=bg).grid(column=0, row=5, sticky=tk.NE)
		tk.Label(self._form, text="To:", bg=bg).grid(column=0, row=6, sticky=tk.NE)

		self._cbx_from = ttk.Combobox(self._form, width=12, state="readonly", values=delay_measurements)
		self._cbx_from.grid(column=1, row=5)
		self._cbx_from.bind("<<ComboboxSelected>>", self._on_selected)
		self._cbx_to = ttk.Combobox(self._form, width=12, state="readonly", values=delay_measurements)
		self._cbx_to.grid(column=1, row=6)
		self._cbx_to.bind("<<ComboboxSelected>>", self._on_selected)

	def _on_selected(self, evt):
		j.setDelaySettings(self.index, self.getFrom(), self.getTo())
		update_panel(self.index)

	def setDelaySettings(self, from_, to):
		self._cbx_from.current(from_ - self._delay_count)
		self._cbx_to.current(to - self._delay_count)

	def getFrom(self):
		return self._cbx_from.current() + self._delay_count

	def getTo(self):
		return self._cbx_to.current() + self._delay_count


mpanels = []
minfos = []
for n in range(jversion.measurements):
	minfos.append(j.getMeasurementInfo(n))

for n in range(jversion.measurements):
	panel = None
	if n < jversion.delays:
		panel = DelayPanel(flow, n, minfos, jversion.delays)
		from_, to = j.getDelaySettings(n)
		panel.setDelaySettings(from_, to)
	else:
		panel = MeasurementPanel(flow, n, minfos)

	mpanels.append(panel)

	flow.window_create(tk.INSERT, window=panel)


def on_save(evt):
	import tkinter.filedialog as fd, csv, itertools
	f = fd.asksaveasfile(parent=root, defaultextension=".csv", filetypes=[("Comma Separated Values", ".csv"), ("All files", "*.*")])
	if f is None:
		return
	with f:
		w = csv.writer(f)
		w.writerow([ "Measurement", "From", "To", "Samples", "Mean", "Min", "Max", "Failures", "Center", "Bin Count", "Bin Width", "Lower", "Higher", "Bin 0" ])

		for p in mpanels:
			if p.open and not p.measurement is None:
				row = []

				m = p.measurement
				h = p.measurement.histogram
				i = minfos[p.index]

				row.append(i.name)

				if p.index < jversion.delays:
					row.append(p.getFrom())
					row.append(p.getTo())
				else:
					row.append("")
					row.append("")

				row.append(m.count)
				row.append(m.mean)
				row.append(m.min)
				row.append(m.max)
				row.append(m.failures)

				row.append(h.center)
				row.append(i.bin_count)
				row.append(h.bin_width)
				row.append(h.lower)
				row.append(h.higher)

				row = [c for c in itertools.chain(row, h.bins)]

				w.writerow(row)

def on_reset_all(evt):
	for p in mpanels:
		if p.open and not p.measurement is None:
			j.reset(p.index)
			update_panel(p.index)

def on_center_all(evt):
	for p in mpanels:
		if p.open and not p.measurement is None:
			j.setHistogram(p.index, p.measurement.mean, p.measurement.histogram.bin_width)
			update_panel(p.index)

if True:
	add_button(toolbar, 2 + 0 * 27, 2, "save", on_save)
	add_button(toolbar, 2 + 1 * 27, 2, "reset", on_reset_all)
	add_button(toolbar, 2 + 2 * 27, 2, "center_mean", on_center_all)


def update_panel(index):
	mpanels[index].update(j.getMeasurement(index))


def update_panels():
	for p in range(len(mpanels)):
		if mpanels[p].open:
			update_panel(p)

	root.after(1000, update_panels)

update_panels()
root.mainloop()

