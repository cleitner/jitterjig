/*
 * dig_output_due.cpp - Digital output for the Arduino Due
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "dig_output.h"

#include "Arduino.h"

void DigOutputDue::begin(void)
{
	/* Output 1: PWM11  PD7/B TIOA8
	 * Output 2: PWM12  PD8/B TIOB8
	 */

	pmc_enable_periph_clk(ID_PIOD);
	PIO_SetPeripheral(PIOD, PIO_PERIPH_B, PIO_PD7 | PIO_PD8);

	pmc_enable_periph_clk(ID_TC8);
	TC_Configure(TC2, 2,
			TC_CMR_TCCLKS_TIMER_CLOCK2 |
			TC_CMR_BURST_NONE |
			TC_CMR_EEVTEDG_NONE |
			TC_CMR_EEVT_XC0 |
			TC_CMR_WAVSEL_UP_RC |
			TC_CMR_WAVE |
			TC_CMR_ACPA_TOGGLE |
			TC_CMR_ACPC_NONE |
			TC_CMR_ASWTRG_CLEAR |
			TC_CMR_BCPB_TOGGLE |
			TC_CMR_BCPC_NONE |
			TC_CMR_BSWTRG_CLEAR);

	/* Period of 500us */
	TC_SetRC(TC2, 2, 4000);

	/* Offset of 100us */
	TC_SetRA(TC2, 2, 801);
	/* Offset of 200us */
	TC_SetRB(TC2, 2, 1601);

	TC_Start(TC2, 2);
}

void DigOutputDue::end(void)
{
	pmc_enable_periph_clk(ID_TC8);
	TC_Stop(TC2, 2);
	pmc_disable_periph_clk(ID_TC8);

	pinMode(11, INPUT);
	pinMode(12, INPUT);
}


