/*
 * sketch.ino - Example Arduino sketch for the JitterJig
 *
 * This file is placed in the public domain.
 */

#include <Scheduler.h>

#include <jitter.h>

void setup()
{
  Jitter.begin();
  
  SerialUSB.begin(115200);
  Serial.begin(115200);
 
  pinMode(22, OUTPUT);
  
  Scheduler.startLoop(squareWave);
}

void loop()
{
  Jitter.loop(SerialUSB);
  yield();
}

void squareWave() {
  digitalWrite(22, HIGH);
  delay(100);
  digitalWrite(22, LOW);
  delay(100);
}

