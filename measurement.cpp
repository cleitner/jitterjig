/*
 * measurement.cpp - Measurement related classes
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "measurement.h"

Measurement::Measurement(void) :
	value_(0),
	count_(0),
	sum_(0),
	min_(UINT32_MAX),
	max_(0),
	histogram_(),
	last_change_(0),
	fail_count_(0)
{
}

void Measurement::clear(void)
{
	count_ = 0;
	value_ = 0;

	sum_count_ = 0;
	sum_ = 0;

	min_ = UINT32_MAX;
	max_ = 0;

	histogram_.clear();

	last_change_ = 0;
	fail_count_ = 0;
}

void Measurement::add(uint32_t value, uint32_t timestamp)
{
	if (count_ == UINT32_MAX) {
		return;
	}

	count_ += 1;
	value_ = value;

	// We have to expect overflow with the high resolution timers we use. At
	// least we can expect a very good mean value
	if (sum_ >= UINT32_MAX / 2) {
		sum_ = get_mean();
		sum_count_ = 1;
	}

	sum_ += value;
	sum_count_ += 1;

	if (value < min_) {
		min_ = value;
	}

	if (value > max_) {
		max_ = value;
	}

	histogram_.add(value);

	last_change_ = timestamp;

	measurementChanged(this);
}

void Measurement::failed(void)
{
	if (fail_count_ < UINT32_MAX) {
		fail_count_ += 1;
	}
}

uint32_t Measurement::get_mean(void) const
{
	if (sum_count_ == 0) {
		return 0;
	}

	if (UINT32_MAX - sum_ < sum_count_ / 2) {
		return sum_ / sum_count_;
	} else {
		return (sum_ + sum_count_ / 2) / sum_count_;
	}
}

MeasurementSink::MeasurementSink(void) :
	measurement_(),
	head_(0),
	tail_(0)
{
}

void MeasurementSink::send(uint32_t id, uint32_t data0, uint32_t data1)
{
	if ((tail_ - head_) % MEASUREMENT_SINK_QUEUE_LENGTH == (MEASUREMENT_SINK_QUEUE_LENGTH - 1)) {
		while (1) {
			__disable_irq();
			Serial.println("mqueue overflowed");
		}
		return;
	}

	uint32_t new_tail = (tail_ + 1) % MEASUREMENT_SINK_QUEUE_LENGTH;

	messages_[new_tail].id = id;
	messages_[new_tail].data0 = data0;
	messages_[new_tail].data1 = data1;

	tail_ = new_tail;
}

bool MeasurementSink::receive(Message* msg)
{
	if (head_ == tail_) {
		return false;
	}

	*msg = messages_[head_];

	head_ = (head_ + 1) % MEASUREMENT_SINK_QUEUE_LENGTH;
	return true;
}

bool MeasurementSink::update(void)
{
	bool changed = false;
	Message msg;

	while (receive(&msg)) {
		changed = true;

		switch (msg.id) {
		case 0:
			measurement_.clear();
			break;
		case 1:
			measurement_.failed();
			break;
		case 2:
			measurement_.add(msg.data0, msg.data1);
			break;
		default:
			assert(0);
			break;
		}
	}

	return changed;
}

extern void __attribute__((weak)) measurementChanged(Measurement* measurement)
{
}

