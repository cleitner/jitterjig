/*
 * dig_input.h - Digital input
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DIG_INPUT_H_
#define DIG_INPUT_H_

#include "input.h"
#include "measurement.h"

#include <stdint.h>

#include "Arduino.h"

class DigInput: public Input
{
protected:
	uint32_t last_edge_;

	Measurement high_;
	Measurement period_;

	void add_edges(uint32_t tsA, uint32_t tsB);

public:
	DigInput(void);
	virtual ~DigInput(void);

	Measurement& get_high(void) { return high_; }
	Measurement& get_period(void) { return period_; }
};

/* -- Implementation on Arduino Due -- */

#include "Arduino.h"

template<int INSTANCE>
class DigInputDue: public DigInput
{
private:
	DigInputDue(const DigInputDue&);
	DigInputDue& operator=(const DigInputDue&);

	const IRQn_Type irq_;
	const uint32_t id_;
	Tc* const tc_;
	const uint32_t ch_;

	void init(void)
	{
		pmc_enable_periph_clk(id_);
		TC_Configure(tc_, ch_,
				TC_CMR_TCCLKS_TIMER_CLOCK2 |
				TC_CMR_BURST_NONE |
				TC_CMR_ETRGEDG_NONE |
				TC_CMR_LDRA_RISING |
				TC_CMR_LDRB_FALLING);

		TC_Start(tc_, ch_);

		/* Synchronize all inputs */
		tc_->TC_BCR = TC_BCR_SYNC;

		TC_GetStatus(tc_, ch_);
		(void) (tc_->TC_CHANNEL + ch_)->TC_RA;
		(void) (tc_->TC_CHANNEL + ch_)->TC_RB;

		(tc_->TC_CHANNEL + ch_)->TC_IER = TC_IER_LOVRS | TC_IER_LDRBS;

		NVIC_EnableIRQ(irq_);
	}

public:
	DigInputDue(void);
	virtual ~DigInputDue(void) { }

	void begin(void);

	void update(void)
	{
		uint32_t status = TC_GetStatus(tc_, ch_);

		uint32_t tra = (tc_->TC_CHANNEL + ch_)->TC_RA;
		uint32_t trb = (tc_->TC_CHANNEL + ch_)->TC_RB;

		if (status & TC_SR_LOVRS) {
			if (!is_failed()) {
				failed();
				high_.failed();
				period_.failed();
			}
		} else {
			if (status & TC_SR_LDRBS) {
				add_edges(tra, trb);
			}
		}
	}
};

#endif /* DIG_INPUT_H_ */

