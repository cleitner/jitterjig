/*
 * jitter.h - JitterJig library
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef JITTER_H_
#define JITTER_H_

#include "DLC.h"
#include "dig_output.h"
#include "dig_input.h"
#include "can_input.h"
//#include "iolink_input.h"

#include "Arduino.h"

/** Nanoseconds per time unit */
extern const uint32_t JITTER_NS_PER_TIME_UNIT;

/**
 * Changes the main clock to 64 MHz.
 */
extern void setupSystemClock(void);

typedef enum {
	JITTER_DELAY1 = 0,
	JITTER_DELAY2,
	JITTER_DELAY3,
	JITTER_DELAY4,
	JITTER_DELAY_COUNT_,
} jitter_delay_t;

typedef enum {
	JITTER_MEASUREMENT_DELAY1 = 0,
	JITTER_MEASUREMENT_DELAY2,
	JITTER_MEASUREMENT_DELAY3,
	JITTER_MEASUREMENT_DELAY4,
	JITTER_MEASUREMENT_DIG1_HIGH,
	JITTER_MEASUREMENT_DIG1_PERIOD,
	JITTER_MEASUREMENT_DIG2_HIGH,
	JITTER_MEASUREMENT_DIG2_PERIOD,
	JITTER_MEASUREMENT_CAN_INTERVAL,
//	JITTER_MEASUREMENT_IOLINK_INTERVAL,
	JITTER_MEASUREMENT_COUNT_,
} jitter_measurement_t;

struct MeasurementInfo {
	const char* name;
	const uint32_t bin_count;

	MeasurementInfo(const char* name, const uint32_t bin_count) :
		name(name),
		bin_count(bin_count)
	{
	}

	MeasurementInfo(const char* name, Measurement& m) :
		name(name),
		bin_count(m.get_histogram().get_bin_count())
	{
	}

};

class Jitter_ {
private:
	struct Delay {
		Delay(void);

		jitter_measurement_t from_index;
		jitter_measurement_t to_index;
		Measurement* from;
		Measurement* to;
		Measurement delay;
		/** Timestamp of last "from" measurement */
		uint32_t last_edge;
	};

	Delay delays_[JITTER_DELAY_COUNT_];

	DigOutputDue dig_output_;
	CANOutput can_output_;
	CANInput can_input_;

	DigInputDue<1> dig_input1_;
	DigInputDue<2> dig_input2_;

	uint32_t dlc_msg_[64];
	DLC dlc_;

	Measurement* measurements_[JITTER_MEASUREMENT_COUNT_];

	void send_ack(Serial_& s);
	void send_nak(Serial_& s);

	friend void measurementChanged(Measurement* measurement);
	friend void TC3_Handler(void);
	friend void TC6_Handler(void);
	friend void TC7_Handler(void);
	friend void CAN0_Handler(void);
	friend void CAN1_Handler(void);

	void on_measurement_changed(Measurement* measurement);

public:
	Jitter_(void);

	void begin(void);
	//void end(void);

	void send(void) { can_output_.send(); }

	void loop(Serial_& s);

	MeasurementInfo get_measurement_info(jitter_measurement_t m) const;

	/** Resets all measurements and fail counters */
	void reset_all(void);

	/** Resets the measurement and the fail counter */
	void reset(jitter_measurement_t m);

	/** Returns a snapshot of the given measurement */
	Measurement get_measurement(jitter_measurement_t m);

	/** Configures (and resets) the histogram for the given measurement */
	void setup_histogram(jitter_measurement_t m, uint32_t center, uint32_t bin_width);

	/** Configures (and resets) the delay measurement */
	bool setup_delay(jitter_delay_t delay, jitter_measurement_t from, jitter_measurement_t to);

	void get_delay_settings(jitter_delay_t delay, jitter_measurement_t* from, jitter_measurement_t* to);
};

extern Jitter_ Jitter;

#endif /* JITTER_H_ */

