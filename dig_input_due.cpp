/*
 * dig_input_due.cpp - Digital output for the Arduino Due
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "dig_input.h"

/* -- Implementation on Arduino Due -- */

template<> DigInputDue<1>::DigInputDue(void) :
	DigInput(),
	irq_(TC7_IRQn),
	id_(ID_TC7),
	tc_(TC2),
	ch_(1)
{
}

template<> void DigInputDue<1>::begin(void)
{
	/* Input 1: PWM3  PC28/B TIOA7
	 */
	pmc_enable_periph_clk(ID_PIOC);
	PIO_SetInput(PIOC, PIO_PC28, PIO_PULLUP);

	init();
}

template<> DigInputDue<2>::DigInputDue(void) :
	DigInput(),
	irq_(TC6_IRQn),
	id_(ID_TC6),
	tc_(TC2),
	ch_(0)
{
}

template<> void DigInputDue<2>::begin(void)
{
	/* Input 2: PWM5  PC25/B TIOA6
	 */
	pmc_enable_periph_clk(ID_PIOC);
	PIO_SetInput(PIOC, PIO_PC25, PIO_PULLUP);

	init();
}

