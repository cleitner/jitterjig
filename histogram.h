/*
 * histogram.h - Histogram class for collecting measurement data
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef HISTOGRAM_H_
#define HISTOGRAM_H_

#include "Arduino.h"

#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

template<size_t BIN_COUNT = 61>
class Histogram
{
private:
	uint32_t center_;
	uint32_t bin_width_;

	uint32_t lower_;
	uint32_t higher_;

	uint32_t bins_[BIN_COUNT];

public:
	Histogram(void)
	{
		setup(0, 1);
	}

	void clear(void)
	{
		lower_ = 0;
		higher_ = 0;

		memset(bins_, 0, 4 * BIN_COUNT);
	}

	void setup(uint32_t center, uint32_t bin_width)
	{
		assert(bin_width > 0);

		this->center_ = center;
		this->bin_width_ = bin_width;

		clear();
	}

	void add(uint32_t value)
	{
		if (value >= center_) {
			uint32_t bin;

			value = value - center_;

			bin = (value + bin_width_ / 2) / bin_width_;

			if (bin > BIN_COUNT / 2) {
				higher_ += 1;
			} else {
				bins_[BIN_COUNT / 2 + bin] += 1;
			}
		} else {
			uint32_t bin;

			value = center_ - value;

			bin = (value + bin_width_ / 2) / bin_width_;

			if (bin > BIN_COUNT / 2) {
				lower_ += 1;
			} else {
				bins_[BIN_COUNT / 2 - bin] += 1;
			}
		}
	}

	uint32_t get_bin_count(void) const { return BIN_COUNT; }

	uint32_t get_center(void) const { return center_; }
	uint32_t get_bin_width(void) const { return bin_width_; }

	uint32_t get_lower(void) const { return lower_; }
	uint32_t get_higher(void) const { return higher_; }

	uint32_t get_bin(int bin) const
	{
		assert(bin >= 0 && bin < BIN_COUNT);
		return bins_[bin];
	}
	uint32_t operator[](int bin) { return get_bin(bin); }

	uint32_t* begin(void) { return &bins_[0]; }
	uint32_t* end(void) { return &bins_[BIN_COUNT]; }

	/** Sum of all bins */
	uint32_t get_total(void) const
	{
		uint32_t total = 0;
		for (int b = 0; b  < BIN_COUNT; b++) {
			total += bins_[b];
		}
		return total;
	}


	void print(Stream& stream)
	{
		int b, n;
		uint32_t total = get_total();

		for (n = 10; n > 0; n--) {
			for (b = 0; b < BIN_COUNT; b++) {
				if (b > 0) {
					stream.print(" | ");
				} else {
					stream.print(" ");
				}
				if (10 * bins_[b] >= total * n) {
					stream.print("#");
				} else {
					stream.print(" ");
				}
			}
			stream.println();
		}

		stream.print("total:\t");
		stream.println(total + lower_ + higher_);
		stream.print("lower:\t");
		stream.println(lower_);
		stream.print("higher:\t");
		stream.println(higher_);

		for (b = 0; b  < BIN_COUNT; b++) {
			uint32_t offset = center_ + (b - BIN_COUNT / 2) * bin_width_;
			int32_t soffset = (int32_t) offset;
			if (soffset < 0) {
				stream.print("-");
			} else {
				stream.print(offset);
			}
			stream.print(":\t");
			stream.println(bins_[b]);
		}
	}
};

#endif /* HISTOGRAM_H_ */

