/*
 * can_input.cpp - CAN input
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "can_input.h"

struct CANSettings {
	uint32_t baudrate;
	/** baudrate prescaler for 64 MHz */
	uint32_t brp;
	uint32_t prop_seg;
	uint32_t phase1;
	uint32_t phase2;

	uint32_t bit_125ns;
};

// We don't use can_init to decide on the settings because this kind of "magic"
// can bite you. These values have a 87.5% sampling point and the maximum SJW
// possible
static const CANSettings CAN_SETTINGS[CAN_BAUDRATE_COUNT_] = {
	{ 1000000,   4, 8, 5, 2, 8 },
	{  800000,   5, 8, 5, 2, 10 },
	{  500000,   8, 8, 5, 2, 16 },
	{  250000,  16, 8, 5, 2, 32 },
	{  125000,  32, 8, 5, 2, 64 },
	{  100000,  40, 8, 5, 2, 80 },
	{   50000,  80, 8, 5, 2, 160 }
};

CANInput::CANInput(Can* can) :
	can_(can),
	baudrate_(CAN_BAUDRATE_100KBPS),
	last_timestamp_(0),
	last_can_timestamp_(0),
	interval_()
{
}

void CANInput::on_irq(uint32_t timestamp)
{
	uint32_t status = can_->CAN_SR;

	if ((can_->CAN_MR & CAN_MR_CANEN) != 1) {
		return;
	}

	if ((status & CAN_SR_FERR) != 0 && !is_reset()) {
		failed();
		// Count every error
		interval_.failed();
	}

	if ((can_->CAN_MB[0].CAN_MSR & CAN_MSR_MRDY) != 0) {
		uint32_t can_timestamp = can_->CAN_MB[0].CAN_MSR & 0xFFFF;

		// Got one
		if (is_reset() || is_failed()) {
			clear_reset();
			clear_failed();
		} else {
			const CANSettings& s = CAN_SETTINGS[baudrate_];

			// TODO: Enable the following line. It enables > 65ms
			// intervals, but should be verified first
			//uint32_t delta = (timestamp - last_timestamp_) / (65536 * s.bit_125ns);
			uint32_t delta = 0;

			interval_.add(((can_timestamp - last_can_timestamp_) % 65536 + delta * 65536) * s.bit_125ns, timestamp);
		}

		last_timestamp_ = timestamp;
		last_can_timestamp_ = can_timestamp;
	}

	// Restart reception
	if (status & CAN_SR_MB0 != 0) {
		rx_next();
	}
}

void CANInput::set_br(void)
{
	const CANSettings& s = CAN_SETTINGS[baudrate_];

	uint32_t sjw = 4;
	if (s.phase1 < sjw) { sjw = s.phase1; }
	if (s.phase2 < sjw) { sjw = s.phase2; }

	can_->CAN_BR =
		CAN_BR_SMP_ONCE |
		CAN_BR_BRP(s.brp - 1) |
		CAN_BR_PROPAG(s.prop_seg - 1) |
		CAN_BR_PHASE1(s.phase1 - 1) |
		CAN_BR_PHASE2(s.phase2 - 1) |
		CAN_BR_SJW(sjw - 1);
}

void CANInput::rx_next(void)
{
	can_->CAN_TCR = CAN_TCR_MB0;
}

void CANInput::begin(can_baudrate_t baudrate)
{
	can_disable(can_);

	baudrate_ = baudrate;

	interval_.clear();
	reset();

	can_->CAN_MR = 0;

	set_br();

	// Setup MB0 with our expected 0x80 message
	can_->CAN_MB[0].CAN_MMR =
		CAN_MMR_MTIMEMARK(0) |
		CAN_MMR_MOT_MB_DISABLED;

	// Only match on exactly 0x80, nothing else
	can_->CAN_MB[0].CAN_MAM = CAN_MAM_MIDvA_Msk;
	can_->CAN_MB[0].CAN_MID = CAN_MID_MIDvA(0x80);

	can_->CAN_MB[0].CAN_MMR = (can_->CAN_MB[0].CAN_MMR & ~CAN_MMR_MOT_Msk) | CAN_MMR_MOT_MB_RX;

	can_->CAN_IER = CAN_IER_MB0 | CAN_IER_FERR;

	rx_next();

	can_enable(can_);
}

CANOutput::CANOutput(Can* can) :
	can_(can)
{
}

void CANOutput::on_irq(void)
{
	uint32_t status = can_->CAN_SR;

	(void) status;
}

void CANOutput::send(void)
{
	can_->CAN_TCR = CAN_TCR_MB0;
}

void CANOutput::begin(can_baudrate_t br)
{
	if (br >= CAN_BAUDRATE_COUNT_) {
		return;
	}

	can_disable(can_);

	const CANSettings& s = CAN_SETTINGS[br];

	uint32_t sjw = 4;
	if (s.phase1 < sjw) { sjw = s.phase1; }
	if (s.phase2 < sjw) { sjw = s.phase2; }

	can_->CAN_BR =
		CAN_BR_SMP_ONCE |
		CAN_BR_BRP(s.brp - 1) |
		CAN_BR_PROPAG(s.prop_seg - 1) |
		CAN_BR_PHASE1(s.phase1 - 1) |
		CAN_BR_PHASE2(s.phase2 - 1) |
		CAN_BR_SJW(sjw - 1);

	can_->CAN_MR = 0;

	// Setup MB0 with our first message
	can_->CAN_MB[0].CAN_MMR =
		CAN_MMR_MTIMEMARK(0) |
		CAN_MMR_MOT_MB_DISABLED;
	can_->CAN_MB[0].CAN_MID = CAN_MID_MIDvA(0x80); // CANopen sync
	can_->CAN_MB[0].CAN_MCR = CAN_MCR_MDLC(0); // No data
	can_->CAN_MB[0].CAN_MMR = (can_->CAN_MB[0].CAN_MMR & ~CAN_MMR_MOT_Msk) | CAN_MMR_MOT_MB_TX;

	// If we need more messages, add them here
	// ...

	can_enable(can_);
}

