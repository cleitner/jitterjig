/*
 * input.h - Input base class
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef INPUT_H_
#define INPUT_H_

#include <stdint.h>

class Input
{
private:
	bool reset_;
	bool failed_;

protected:
	void reset(void);
	void failed(void);
	
	bool is_failed(void) const { return failed_; }
	bool is_reset(void) const { return reset_; }

	void clear_failed(void) { failed_ = false; }
	void clear_reset(void) { reset_ = false; }

public:
	Input(void);
	virtual ~Input(void);
};

#endif /* INPUT_H_ */

