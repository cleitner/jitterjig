TODO
====

 * Remove all remaining code TODOs
 * Delays between CAN intervals and digital inputs have weird values - why?
 * Document for each input where we sample which timestamp, e.g. SOF/EOF for CAN
 * Enable the timer assisted CAN interval code that allows for intervals
   greater than the 0xFFFF times baud-rate
 * Separate some Arduino specific parts a bit better or ditch the concept of
   supporting other devices right away
 * Add IO-Link support
 * Add Ethernet support - that'll require supporting other devices, because
   Arduino chose not to add a pin header for the integrated MAC - bummer.
 * Remove the OC flag for the CAN output
 * Add an image/schematic for the Arduino Due pinout - I tend to forget which
   pins to connect :)

Interface
---------

 * Finish CLI, which must support generating a histogram PNG for regression
   testing
 * Maybe save the visibility of the measurement panels, because we tend to
   always close the same panels on startup, which is cumbersome
 * Add vertical scale to the histogram

