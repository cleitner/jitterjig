/*
 * measurement.h - Measurement related classes
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MEASUREMENT_H_
#define MEASUREMENT_H_

#include "histogram.h"

#include <functional>

#include <stdint.h>

class Measurement {
private:
	/** last value */
	uint32_t value_;

	/** number of samples */
	uint32_t count_;

	/** number of samples in sum */
	uint32_t sum_count_;
	/** sum of all samples */
	uint64_t sum_;

	uint32_t min_;
	uint32_t max_;

	Histogram<> histogram_;

	/** last change for delay measurement */
	uint32_t last_change_;

	/** Failure counter */
	uint32_t fail_count_;

	friend class Input;

public:
	Measurement(void);

	void clear(void);
	void failed(void);

	void add(uint32_t value, uint32_t timestamp);

	uint32_t get_last_value(void) const { return value_; }
	uint32_t get_last_change(void) const { return last_change_; }
	uint32_t get_count(void) const { return count_; }
	uint32_t get_mean(void) const;
	uint32_t get_min(void) const { return min_; }
	uint32_t get_max(void) const { return max_; }
	uint32_t get_fail_count(void) const { return fail_count_; }

	// Return a reference, not a copy, because you'll have to copy the
	// entire measurement anyway to get a consistent view of the current
	// state
	Histogram<>& get_histogram(void) { return histogram_; }

	void print(Stream& stream)
	{
		histogram_.print(stream);
		stream.print("mean:\t");
		stream.println(get_mean());
		stream.print("min:\t");
		stream.println(get_min());
		stream.print("max:\t");
		stream.println(get_max());
	}
};

#define MEASUREMENT_SINK_QUEUE_LENGTH 128

/**
 * The measurement sink can be used as a lockless variant of the measurement
 * class. @c MeasurementSource can be used to add values from an interrupt
 * handler, which are processed by @c update in the update task, usually the
 * main loop.
 *
 * Beware that the interrupt handler <b>can't</b> access the sink state itself
 * unless calling update first.
 */
class MeasurementSink
{
private:
	friend class MeasurementSource;

	// Messages from interrupt handlers. 
	struct Message {
		uint32_t id;
		uint32_t cmd;
		uint32_t data0;
		uint32_t data1;
	};

	Message messages_[MEASUREMENT_SINK_QUEUE_LENGTH];

	Measurement measurement_;
	uint32_t head_;
	uint32_t tail_;

	void send(uint32_t id, uint32_t data0, uint32_t data1);
	bool receive(Message* msg);

public:
	MeasurementSink(void);

	/**
	 * Updates the measurement with all values sent by the source
	 *
	 * @return @c true if changes have been made
	 */
	bool update(void);

	Measurement& get_measurement(void) { return measurement_; }
};

class MeasurementSource
{
private:
	MeasurementSink& sink_;

public:
	MeasurementSource(MeasurementSink& sink) : sink_(sink) { }

	void clear(void) { sink_.send(0, 0, 0); }
	void failed(void) { sink_.send(1, 0, 0); }
	void add(uint32_t value, uint32_t timestamp) { sink_.send(2, value, timestamp); }
};



extern void measurementChanged(Measurement* measurement);

#endif /* MEASUREMENT_H_ */

