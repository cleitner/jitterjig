/*
 * input.cpp - Input base class
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "input.h"

Input::Input(void) :
	reset_(true),
	failed_(true)
{
}

Input::~Input(void)
{
}

void Input::reset(void)
{
	reset_ = true;
	failed_ = false;
}

void Input::failed(void)
{
	if (reset_) {
		return;
	}

	failed_ = true;
}

