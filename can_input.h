/*
 * can_input.h - CAN input
 *
 * Copyright (c) 2013, 2014 Colin Leitner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CAN_INPUT_H_
#define CAN_INPUT_H_

#include "input.h"
#include "measurement.h"

#include "Arduino.h"

/**
 * The CAN inputs assume a 64 MHz main clock
 */

typedef enum {
	CAN_BAUDRATE_1MBPS = 0,
	CAN_BAUDRATE_800KBPS,
	CAN_BAUDRATE_500KBPS,
	CAN_BAUDRATE_250KBPS,
	CAN_BAUDRATE_125KBPS,
	// 100 kBit/s is not a CANopen baudrate
	CAN_BAUDRATE_100KBPS,
	CAN_BAUDRATE_50KBPS,
	// we would have to slow down our main clock further to support 25 and
	// 10 kBit/s. Not worth it, most devices don't support these speeds
	// anyway (presumly for the same reason)
	CAN_BAUDRATE_COUNT_
} can_baudrate_t;

/**
 * CAN input, which samples CANopen messages with ID 0x80
 */
class CANInput: public Input {
private:
	Can* can_;

	can_baudrate_t baudrate_;

	uint32_t last_timestamp_;
	uint32_t last_can_timestamp_;

	Measurement interval_;

	void set_br(void);
	void rx_next(void);

public:
	CANInput(Can* can);

	void on_irq(uint32_t timestamp);

	void begin(can_baudrate_t br);

	can_baudrate_t get_baudrate(void) { return baudrate_; }

	Measurement& get_interval(void) { return interval_; }
};

/**
 * Sends a CANopen SYNC frame in regular intervals.
 */
class CANOutput {
private:
	Can* can_;

public:
	CANOutput(Can* can);

	void on_irq(void);

	void send(void);

	void begin(can_baudrate_t br);
};

#endif /* CAN_INPUT_H_ */

